# ultimateCrashLoopback para sistemas Kubernetes

## Script que deleta pods em estado "CrashLoopBackOff"

Modo de usar:

1. Ajuste as variáveis "namespaces" e "status" de acordo com a necessidade.
2. Execute o script em um ambiente com kube-config com permissões de raquetar pods:
```
./ultimateCrashLoopbackOff.sh
```