#!/bin/bash

namespaces="dev hmg pre"
status="CrashLoopBackOff"
for i in $namespaces
do
kubectl get pods -n $i | grep $status | awk '{print $1}' | xargs kubectl delete pod -n $i
done